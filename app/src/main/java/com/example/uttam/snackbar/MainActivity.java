package com.example.uttam.snackbar;

import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.like.LikeButton;
import com.like.OnLikeListener;

import br.com.bloder.magic.view.MagicButton;

public class MainActivity extends AppCompatActivity {
    Button snackBtn,snackBtn2;
    MagicButton fbBtn, twBtn, ytBtn;
    LikeButton heartBtn,thumbBtn,starBtn;
    String arrayName[]={"Facebook","Twitter","Windows","Apple","Youtube","Google"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CircleMenu ciecleMenu=findViewById(R.id.circleMenuId);
        ciecleMenu.setMainMenu(Color.parseColor("#cdcdcd"),R.drawable.add,R.drawable.cross)
                .addSubMenu(Color.parseColor("#4484e4"),R.drawable.facebook)
                .addSubMenu(Color.parseColor("#d6dfed"),R.drawable.twitter_icon)
                .addSubMenu(Color.parseColor("#33f195"),R.drawable.icon_windows)
                .addSubMenu(Color.parseColor("#9195ce"),R.drawable.icon_apple)
                .addSubMenu(Color.parseColor("#e4444f"),R.drawable.youtube_icon)
                .addSubMenu(Color.parseColor("#83d1fd"),R.drawable.google)
                .setOnMenuSelectedListener(new OnMenuSelectedListener() {
                    @Override
                    public void onMenuSelected(int i) {
                        Toast.makeText(MainActivity.this, arrayName[i], Toast.LENGTH_SHORT).show();
                    }
                });


        heartBtn=findViewById(R.id.heart_buttonId);
        thumbBtn=findViewById(R.id.thumb_buttonId);
        starBtn=findViewById(R.id.star_buttonId);

        heartBtn.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                Toast.makeText(MainActivity.this, "red heart", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                Toast.makeText(MainActivity.this, "gray heart", Toast.LENGTH_SHORT).show();
            }
        });

        thumbBtn.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                Toast.makeText(MainActivity.this, "blue like", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                Toast.makeText(MainActivity.this, "gray like", Toast.LENGTH_SHORT).show();
            }
        });

        starBtn.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                Toast.makeText(MainActivity.this, "yellow star", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                Toast.makeText(MainActivity.this, "gray star", Toast.LENGTH_SHORT).show();
            }
        });

        snackBtn=findViewById(R.id.snackBarId);
        snackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSnackbar();
            }
        });

        snackBtn2=findViewById(R.id.snackBarId2);
        snackBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSnackbar2(v);
            }
        });

        fbBtn=findViewById(R.id.fbBtnId);
        fbBtn.setMagicButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "facebook", Toast.LENGTH_SHORT).show();
            }
        });

        ytBtn=findViewById(R.id.ytBtnId);
        ytBtn.setMagicButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "youtube", Toast.LENGTH_SHORT).show();
            }
        });

        twBtn=findViewById(R.id.twBtnId);
        twBtn.setMagicButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "twitter", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //snackbar
    private void createSnackbar() {
        CoordinatorLayout coordinatorLayout=findViewById(R.id.coordId);

        Snackbar snackbar=Snackbar.make(coordinatorLayout,"Making Sanckbar Successful",Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(this,"snackbar",Toast.LENGTH_SHORT).show();
                    }
                })
                .setActionTextColor(Color.GREEN);

        View snackView=snackbar.getView();
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
    //snackbar
    private void createSnackbar2(View v) {
        //LinearLayout linearLayout=findViewById(R.id.linearId);

//        Snackbar snackbar=Snackbar.make(v,"Making Sanckbar 2 Successful",Snackbar.LENGTH_LONG)
//                .setAction("OK", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        //Toast.makeText(this,"snackbar",Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .setActionTextColor(Color.GREEN);
//
//
//        View snackView=snackbar.getView();
//        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.YELLOW);
//        snackbar.show();

        Snackbar snackbar = Snackbar.make(v, "message", Snackbar.LENGTH_LONG);
        View snackbarLayout = snackbar.getView();
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );

        //lp.setMargins(50, 0, 0, 0);
        snackbarLayout.setLayoutParams(lp);
        snackbar.show();
    }
}
